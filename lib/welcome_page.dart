// import 'package:flutter/material.dart';
// import 'package:homeloan/coapp1_details.dart';
// import 'package:homeloan/models/user.dart';


// class WelcomePage extends StatelessWidget {
//   User user;

//   WelcomePage({
//     Key? key,
//     required this.user,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SingleChildScrollView(
//         child: Center(
//           child: Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 const SizedBox(height: 40),
//                 const Text('General Details',
//                     style:
//                         TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
//                 Text('Name:  ${user.name}'),
//                 Text('Gender:  ${user.gender}'),
//                 Text('DOB:  ${user.dob}'),
//                 Text('Marital Status:  ${user.marital}'),
//                 Text('Email:  ${user.email}'),
//                 Text('Mobile No.: ${user.mob}'),
//                 Text('Adhar: ${user.adhar}'),
//                 Text('Pan: ${user.pan}'),
//                 Text('Source: ${user.source}'), //personal info

//                 const SizedBox(height: 40),
//                 const Text('Address',
//                     style:
//                         TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
//                 Text('Current Address: ${user.caddress}'),
//                 Text('Pin Code: ${user.cpincode}'),
//                 Text('State: ${user.cstate}'),
//                 Text('City: ${user.ccity}'),
//                 Text('Permanent Address: ${user.paddress}'),
//                 Text('Pin Code : ${user.ppincode}'),
//                 Text('State: ${user.pstate}'),
//                 Text('City: ${user.pcity}'), // address  info

//                 const SizedBox(height: 40),
//                 const Text('Professional Information',
//                     style:
//                         TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),

//                 Text('Employment Type: ${user.emptype}'),
//                 Text('Employer Type: ${user.employertype}'),
//                 Text('Company Name: ${user.companyname}'),
//                 Text('Employer Address: ${user.empaddress}'),
//                 Text('Pin Code: ${user.epincode}'),
//                 Text('State: ${user.estate}'),
//                 Text('City : ${user.empaddress}'),
//                 Text('Official Email: ${user.officialemail}'),
//                 Text(
//                     'Overall Work Experience (In Years): ${user.empexperience}'),
//                 Text('Monthly Income: ${user.monthlyincome}'),
//                 Text('Existing EMI: ${user.existingemi}'),
//                 Text('Annual Income: ${user.annualincome}'),
//                 Text(
//                     'Annual Profit: ${user.annualprofit}'), // employement details

//                 const SizedBox(height: 40),
//                 const Text('Loan Requirements',
//                     style:
//                         TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),

//                 Text('Product: ${user.product}'),
//                 Text('Amount Required: ${user.amnt}'),
//                 Text('Loan Tenure: ${user.loantenure}'), // Loan requirements

//                 const SizedBox(height: 40),
//                 const Text('Property Details',
//                     style:
//                         TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),

//                 Text('Project Name: ${user.projname}'),
//                 Text('Property Status: ${user.propertystatus}'),
//                 Text('Property Value: ${user.propertyvalue}'),
//                 Text('Pin Code: ${user.pdpincode}'),
//                 Text('State: ${user.pdstate}'),
//                 Text('City : ${user.pdcity}'), // property details

//                 const SizedBox(height: 40),
//                 const Text('Documents',
//                     style:
//                         TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),

//                 Text('Bank Statement:${user.bankdoc}'),
//                 Text('Salary Slip: ${user.salaryslip}'),
//                 Text('ITR Form 16: ${user.itrform}'),
//                 Text('Aadhar: ${user.aadhardoc}'),
//                 Text('Pan: ${user.pandoc}'),
//                 Text('Others: ${user.othersdoc}'), //documents

//                 const SizedBox(height: 20),

//                 ElevatedButton(
//                   onPressed: (){
//                     Navigator.push(context,
//                         MaterialPageRoute(builder: (_) =>  Coapp1Details()));
//                   },
//                   child:Text('data'),
//                 )
               
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
