import 'package:flutter/material.dart';
import 'package:homeloan/widgets/drawer.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf3fbfe),
      key: _key,
      endDrawer: const MyDrawer(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Container(
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: const Color(0xFFf3fbfe),
                // borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: const Offset(1, 1)),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'assets/images/KuberX.png',
                    width: 80,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: InkWell(
                          onTap: () {
                            _key.currentState!.openEndDrawer();
                          },
                          child: const Icon(Icons.menu))),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: const [
              Padding(padding: EdgeInsets.only(left: 20)),
              Text('Dashboard',
                  style: TextStyle(fontSize: 24, color: Color(0xFF152245))),
            ]),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: MaterialButton(
                            height: 100.0,
                            minWidth: 150.0,
                            color: const Color(0xFFf3fbfe),
                            textColor: const Color(0xFF152245),
                            child: Column(
                              children: const [
                                Text(
                                  "Total Leads",
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                                SizedBox(height: 10),
                                Text(
                                  '0',
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                            onPressed: () => {},
                            splashColor: const Color(0xFF05bd8e),
                          )),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: MaterialButton(
                              height: 100.0,
                              minWidth: 150.0,
                              color: const Color(0xFFf3fbfe),
                              textColor: const Color(0xFF152245),
                              child: Column(
                                children: const [
                                  Text(
                                    "Rejected Leads",
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    '0',
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () => {},
                              splashColor: const Color(0xFF05bd8e))),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: MaterialButton(
                              height: 100.0,
                              minWidth: 150.0,
                              color: const Color(0xFFf3fbfe),
                              textColor: const Color(0xFF152245),
                              child: Column(
                                children: const [
                                  Text(
                                    "Disbursement No :",
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    '0',
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () => {},
                              splashColor: const Color(0xFF05bd8e))),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: MaterialButton(
                              height: 100.0,
                              minWidth: 150.0,
                              color: const Color(0xFFf3fbfe),
                              textColor: const Color(0xFF152245),
                              child: Column(
                                children: const [
                                  Text(
                                    "Disbursement Amt :",
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    '-',
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () => {},
                              splashColor: const Color(0xFF05bd8e))),
                    ],
                  ),
                ),
              ],
            )
          ]),
        ),
      ),
    );
  }
}
