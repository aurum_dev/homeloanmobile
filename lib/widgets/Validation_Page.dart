import 'package:flutter/material.dart';

class CommonFunction {
  static String? validationpage(value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}

class DropFunction {
  static String? dropvalidation(value) {
    if (value == null || value.isEmpty) {
      return 'Please select an option';
    }
    return null;
  }
}

//

class Constants {
  static ButtonStyle buttonStyle = ElevatedButton.styleFrom(
      primary: Colors.white, // background (button) color
      onPrimary: const Color(0xFF05bd8e),
      fixedSize: const Size(150, 40),
      side: const BorderSide(color: Color(0xFF05bd8e)));
}
//submit and Cancel button
class Sumbitcancel {
  static ButtonStyle buttonStyle1 = ElevatedButton.styleFrom(
      primary: const Color(0xFF152245), // background (button) color
      onPrimary: Colors.white, // foreground (text) color
      fixedSize: const Size(150, 40));
}

