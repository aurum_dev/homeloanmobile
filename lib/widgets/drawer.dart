import 'package:flutter/material.dart';
import 'package:homeloan/coapp1_details.dart';
import 'package:homeloan/dashboard_page.dart';
import 'package:homeloan/home_page.dart';
import 'package:homeloan/splash_screen.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: const Color(0xFFf3fbfe),
      child: ListView(children: [
        const SizedBox(height: 20),
        ListTile(
          leading: const Icon(Icons.home),
          title: const Text('Dashboard'),
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => const DashboardPage()));
          },
        ),
        ListTile(
          leading: const Icon(Icons.ac_unit),
          title: const Text('Customer Leads'),
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Homepage()));
          },
        ),
        ListTile(
            leading: const Icon(Icons.logout_outlined),
            title: const Text('Logout'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => const SplashScreen()));
            })
      ]),
    );
  }
}
