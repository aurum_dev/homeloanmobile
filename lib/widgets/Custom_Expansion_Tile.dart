import 'package:flutter/material.dart';

class CustomExpansionTile extends StatelessWidget {
  const CustomExpansionTile(
      {Key? key, required this.title, required this.children})
      : super(key: key);
  final Widget title;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      // backgroundColor:Colors.white,
      collapsedBackgroundColor: const Color(0xFF05bd8e),
      backgroundColor: const Color(0xFF05bd8e),
      collapsedIconColor: Colors.white,
      iconColor: Colors.white,
      maintainState: true,
      title: title,
      children: children,
    );
  }
}
