import 'dart:async';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:homeloan/login_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

@override
  void initState(){
   super.initState();
   Timer(Duration(seconds: 7),  
            ()=>Navigator.pushReplacement(context,  
            MaterialPageRoute(builder:  
                (context) => LoginPage()  
            )
            )
   );
   
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(45.0),
        child: Container(
            alignment: Alignment.center,
            // width: 150,
            child: Image.asset('assets/images/KuberX_Logo.gif')),
      ),
    );
  }
}
