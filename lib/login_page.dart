import 'package:flutter/material.dart';
// import 'package:homeloan/first_screen.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:homeloan/forgot_password.dart';
import 'package:homeloan/home_page.dart';
import 'package:homeloan/register.dart';
import 'package:homeloan/register_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf3fbfe),
      body: ListView(
        children: [
          Container(
            width: 150,
            height: 150,
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 40),
            child: Image.asset(
              'assets/images/KuberX.png',
              width: 150,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 35),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Login',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 35),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Please Sign in to continue.',
                  style: TextStyle(fontSize: 15),
                )
              ],
            ),
          ),
          SizedBox(height: 30),
          Form(
            key: formkey,
            child: Container(
              padding: EdgeInsets.only(
                  // top: MediaQuery.of(context).size.height * 0.1,
                  right: 35,
                  left: 35),
              child: Column(
                children: [
                  TextFormField(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email',
                          hintText: 'Enter valid email id as abc@gmail.com'),
                      validator: MultiValidator([
                        RequiredValidator(errorText: "* Required"),
                        EmailValidator(errorText: "Enter valid email id"),
                      ])),
                  SizedBox(height: 20),
                  TextFormField(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Password',
                          hintText: 'Enter secure password'),
                      validator: MultiValidator([
                        RequiredValidator(errorText: "* Required"),
                        MinLengthValidator(6,
                            errorText:
                                'Password should be atleast 6 characters'),
                        MaxLengthValidator(15,
                            errorText:
                                'Password should not be greater than 15 characters')
                      ])),
                  SizedBox(height: 20),

                  ForgotPassword(), //forgot Password

                  SizedBox(height: 15),
                  Container(
                    // width: 300,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary:
                              Color(0xFF152245), // background (button) color
                          onPrimary: Colors.white, // foreground (text) color
                          fixedSize: Size(400, 50)),
                      onPressed: (() {
                        if (formkey.currentState!.validate()) {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) => Homepage()));
                          print("Validated");
                        } else {
                          print("Not Validated");
                        }
                      }),
                      child: Text(
                        'LOGIN',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),

                  Register(), //register
                ],
              ),
            ),
          ),
        ],
      ),
    );
    // return Scaffold(
    //   backgroundColor: Colors.white,
    //   body: Stack(
    //     children: [
    //       Padding(
    //         padding: EdgeInsets.all(20),
    //         child: Container(
    //           height: 100,
    //           width: 100,
    //           decoration: const BoxDecoration(
    //               image: DecorationImage(
    //             image: AssetImage(
    //               "assets/images/KuberX.png",
    //             ),
    //             fit: BoxFit.contain,
    //           )),
    //           // padding: EdgeInsets.only(left: 40, top: 130),
    //           // child: Text(
    //           //   "Welcome Back",
    //           //   style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
    //           // ),
    //         ),
    //       ),
    //       SingleChildScrollView(
    //         child: Form(
    //           key: formkey,
    //           child: Container(
    //               padding: EdgeInsets.only(
    //                   top: MediaQuery.of(context).size.height * 0.5,
    //                   right: 35,
    //                   left: 35),
    //               child: Column(
    //                 children: [
    //                   TextFormField(
    //                       decoration: const InputDecoration(
    //                           border: OutlineInputBorder(),
    //                           labelText: 'Email',
    //                           hintText:
    //                               'Enter valid email id as abc@gmail.com'),
    //                       validator: MultiValidator([
    //                         RequiredValidator(errorText: "* Required"),
    //                         EmailValidator(errorText: "Enter valid email id"),
    //                       ])),
    //                   SizedBox(height: 30),
    //                   TextFormField(
    //                       // keyboardType:TextInputType.number,
    //                       obscureText: true,
    //                       decoration: const InputDecoration(
    //                           border: OutlineInputBorder(),
    //                           labelText: 'Password',
    //                           hintText: 'Enter secure password'),
    //                       validator: MultiValidator([
    //                         RequiredValidator(errorText: "* Required"),
    //                         MinLengthValidator(6,
    //                             errorText:
    //                                 "Password should be atleast 6 characters"),
    //                         MaxLengthValidator(15,
    //                             errorText:
    //                                 "Password should not be greater than 15 characters")
    //                       ])),
    //                   SizedBox(height: 40),
    //                   Row(
    //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                     children: [
    //                       Text(
    //                         "Sign In",
    //                         style: TextStyle(
    //                             fontSize: 30,
    //                             fontWeight: FontWeight.w700,
    //                             color: Colors.black),
    //                       ),
    //                       CircleAvatar(
    //                           radius: 30,
    //                           backgroundColor: Colors.black,
    //                           child: IconButton(
    //                             onPressed: () {
    //                               if (formkey.currentState!.validate()) {
    //                                 Navigator.push(
    //                                     context,
    //                                     MaterialPageRoute(
    //                                         builder: (_) => Homepage()));
    //                                 print("Validated");
    //                               } else {
    //                                 print("Not Validated");
    //                               }
    //                             },
    //                             icon: Icon(Icons.arrow_forward),
    //                           ))
    //                     ],
    //                   ),
    //                   Row(
    //                     children: [
    //                       TextButton(
    //                           onPressed: () {
    //                             Navigator.pushNamed(context, 'register');
    //                           },
    //                           child: Text('Sign Up',
    //                               style: TextStyle(
    //                                 fontSize: 16,
    //                                 fontWeight: FontWeight.bold,
    //                                 decoration: TextDecoration.underline,
    //                               ))),
    //                       TextButton(
    //                           onPressed: () {
    //                             Navigator.pushNamed(context, 'forgotpassword');
    //                           },
    //                           child: Text('Forgot Password',
    //                               style: TextStyle(
    //                                 fontSize: 16,
    //                                 fontWeight: FontWeight.bold,
    //                                 decoration: TextDecoration.underline,
    //                               )))
    //                     ],
    //                   )
    //                 ],
    //               )),
    //         ),
    //       )
    //     ],
    //   ),
    // );
  }
}
