import 'package:flutter/material.dart';

class AlreadyAccount extends StatelessWidget {
  const AlreadyAccount({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          'Already have a account?',
          style: TextStyle(
            fontSize: 16,
          ),
        ),
        TextButton(
          onPressed: () {
            Navigator.pushNamed(context, 'login');
          },
          child: const Text(
            'LOGIN',
            style: TextStyle(
                fontSize: 16,
                color: Color(0xFF05bd8e),
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
        )
      ],
    );
  }
}
