import 'dart:developer';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:homeloan/widgets/Custom_Expansion_Tile.dart';
import 'package:homeloan/coapp1_details.dart';
import 'package:homeloan/file_list.dart';
import 'package:homeloan/login_page.dart';
import 'package:homeloan/models/user.dart';
import 'package:homeloan/service/service.dart';
import 'package:homeloan/welcome_page.dart';
import 'package:homeloan/widgets/Validation_Page.dart';
import 'package:homeloan/widgets/bold_text.dart';
import 'package:homeloan/widgets/drawer.dart';
import 'package:homeloan/widgets/headline_text.dart';
import 'package:open_file/open_file.dart';

class Homepage extends StatefulWidget {
  Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  TextEditingController _name = TextEditingController();
  TextEditingController _gender = TextEditingController();
  TextEditingController _dob = TextEditingController();
  TextEditingController _marital = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _mob = TextEditingController();
  TextEditingController _adhar = TextEditingController();
  TextEditingController _pan = TextEditingController();
  TextEditingController _source = TextEditingController(); //personal info

  TextEditingController _caddress = TextEditingController();
  TextEditingController _cpincode = TextEditingController();
  TextEditingController _cstate = TextEditingController();
  TextEditingController _ccity = TextEditingController(); // current address

  TextEditingController _paddress = TextEditingController();
  TextEditingController _ppincode = TextEditingController();
  TextEditingController _pstate = TextEditingController();
  TextEditingController _pcity = TextEditingController(); //permanent address

  TextEditingController _emptype = TextEditingController();
  TextEditingController _employertype = TextEditingController();
  TextEditingController _companyname = TextEditingController();
  TextEditingController _empaddress = TextEditingController();
  TextEditingController _epincode = TextEditingController();
  TextEditingController _estate = TextEditingController();
  TextEditingController _ecity = TextEditingController();
  TextEditingController _officialemail = TextEditingController();
  TextEditingController _empexperience = TextEditingController();
  TextEditingController _monthlyincome = TextEditingController();
  TextEditingController _existingemi = TextEditingController();
  TextEditingController _annualincome = TextEditingController();
  TextEditingController _annualprofit =
      TextEditingController(); //Prfessional Info

  TextEditingController _product = TextEditingController();
  TextEditingController _amnt = TextEditingController();
  TextEditingController _loantenure =
      TextEditingController(); //loan requirements

  TextEditingController _projname = TextEditingController();
  TextEditingController _propertystatus = TextEditingController();
  TextEditingController _propertyvalue = TextEditingController();
  TextEditingController _pdpincode = TextEditingController();
  TextEditingController _pdstate = TextEditingController();
  TextEditingController _pdcity = TextEditingController(); // property details

  TextEditingController _bankdoc = TextEditingController(); //document detials
  TextEditingController _salaryslip = TextEditingController();
  TextEditingController _itrform = TextEditingController();
  TextEditingController _gst = TextEditingController();
  TextEditingController _aadhardoc = TextEditingController();
  TextEditingController _pandoc = TextEditingController();
  TextEditingController _othersdoc = TextEditingController();

  String fileType = 'All';

  var fileTypeList = ['All', 'Multiple'];

  FilePickerResult? result;
  PlatformFile? file,
      bankfile,
      salaryfile,
      // gstfile,
      itrfile,
      aadharfile,
      panfile,
      othersfile;

  // bool? isAdhar;
  // bool? isPan;
  // bool isName = true;
  // bool? isMbl;

  // var adharValue;

  final formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf3fbfe),
      key: _key,
      endDrawer: const MyDrawer(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Container(
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: const Color(0xFFf3fbfe),
                // borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: const Offset(1, 1)),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'assets/images/KuberX.png',
                    width: 80,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: InkWell(
                          onTap: () {
                            _key.currentState!.openEndDrawer();
                          },
                          child: const Icon(Icons.menu))),
                ],
              ),
            ),
            Form(key: formKey, child: _buildBody(context)),
          ]),
        ),
      ),
    );
  }

  SingleChildScrollView _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 20),
          const Text(
            'Primary Applicant',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          _generalDetails(context),
          const SizedBox(height: 10),
          _addressDetails(context),
          const SizedBox(height: 10),
          _professionalDetails(context),
          const SizedBox(height: 10),
          _loanrequirementDetails(context),
          const SizedBox(height: 10),
          _propertyDetails(context),
          const SizedBox(height: 10),
          _docdetails(context),
/*=============================================two button=============================================*/
          const SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: Sumbitcancel.buttonStyle1,
                //  ElevatedButton.styleFrom(
                //     primary:
                //         const Color(0xFF152245), // background (button) color
                //     onPrimary: Colors.white, // foreground (text) color
                //     fixedSize: const Size(150, 40)),
                // autofocus:false,
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const LoginPage()));
                },
                child: const Text(
                  'CANCEL',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(width: 40),
              ElevatedButton(
                style: Sumbitcancel.buttonStyle1,
                // ElevatedButton.styleFrom(
                //     primary:
                //         const Color(0xFF152245), // background (button) color
                //     onPrimary: Colors.white, // foreground (text) color
                //     fixedSize: const Size(150, 40)),
                onPressed: () {
                  // isPan ??= false;
                  // log("PAN: $isPan"); //pan

                  // isAdhar ??= false;
                  // log("Aadhar: $isAdhar"); // aadhar

                  // isMbl ??= false;
                  // log("Mobile: $isMbl"); //mobile

                  setState(() {});
                  log("${formKey.currentState!.validate()}");

                  if (formKey.currentState!.validate()) {
                    getApiData(context); //called api

                    APIService.uploadDocs(bankfile, salaryfile, itrfile,
                        aadharfile, panfile, othersfile);
                  } else {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                          title: const Text('AlertDialog Title'),
                          content: const Text('All Fields Required'),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.pop(context, 'OK');
                                },
                                child: const Text('OK',
                                    style: TextStyle(
                                      color: Color(0xFF152245),
                                    ))),
                          ]),
                    );
                  }
                },
                child: const Text(
                  'SUBMIT',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  /*=====================================General Details===============================================*/
  CustomExpansionTile _generalDetails(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(
            Icons.people,
            color: Colors.white,
          ),
          SizedBox(width: 20),
          HeadlineText(
            text: "General Details",
          ),
          // Text(
          //   'General Details',
          //   style: TextStyle(
          //     color: Colors.white,
          //     fontWeight: FontWeight.bold,
          //   ),
          // ),
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _name,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Full Name'),
                    hintText: 'Enter Full Name',
                  ),
                  validator: CommonFunction.validationpage, //
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _gender,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Gender',
                          hintText: 'Select an option')),
                  suggestionsCallback: (pattern) {
                    return ['Female', 'Male', 'Others'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _gender.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please select an option';
                  //   }
                  //   return null;
                  // },
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  onTap: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1982),
                      lastDate: DateTime(2023),
                    );
                    // intl
                    _dob.text = "$date";
                  },
                  controller: _dob,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('D.O.B'),
                    hintText: 'Enter Date of Birth',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _marital,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Marital Status',
                          hintText: 'Select an option')),
                  suggestionsCallback: (pattern) {
                    return ['Married', 'Single', 'Divorced'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _marital.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _email,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Email'),
                    hintText: 'Report will be sent here',
                  ),
                  validator: CommonFunction.validationpage,
                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please enter some text';
                  //   }
                  //   return null;
                  // },
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _mob,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Mobile No.'),
                    hintText: 'Enter Mobile No.',
                    // errorText: (isMbl ?? true) ? null : "Enter Mobile No.?"
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter digit';
                    }
                    if (value.length != 10 ||
                        !RegExp(r'^[0-9]{10}$').hasMatch(value)) {
                      return 'Enter Valid Mobile No.';
                    }

                    return null;
                  },

                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please enter digit';
                  //   }

                  //   return null;
                  // }

                  // onChanged: (value) {
                  //   if (value.isEmpty ||
                  //       RegExp(r'^[0-9]{10}$').hasMatch(value)) {
                  //     isMbl = true;
                  //   } else {
                  //     isMbl = false;
                  //   }

                  //   _mob.text;
                  //   setState(() {});
                  // }
                ),
                // validator: (value) {
                //   if (value == null || value.isEmpty) {
                //     return 'Please enter some text';
                //   }
                //   const pattern = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
                //   final regExp = RegExp(pattern);

                //   if (!regExp.hasMatch(value)) {
                //     return 'Please enter valid number';
                //   } else {
                //     return 'number';
                //   }
                // },
                // ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: _adhar,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('Aadhar No.'),
                      hintText: 'Enter 12 digit aadhar number',
                      // errorText: (isAdhar ?? true)
                      //     ? null
                      //     : "Aadhar not validator ?"
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Digit';
                      }
                      if (value.length != 12 ||
                          !RegExp(r'^[2-9]{1}[0-9]{11}$').hasMatch(value)) {
                        return 'Enter Valid Aadhar No.';
                      }

                      return null;
                    }
                    // onChanged: (value) {
                    //   // String patttern = r'/^[2-9]{1}[0-9]{3}\s{1}[0-9]{4}\s{1}[0-9]{4}$/';
                    //   if (value.isEmpty) {
                    //     isAdhar = false;
                    //   }
                    //   if (value.length == 12) {
                    //     isAdhar = true;
                    //   } else {
                    //     isAdhar = false;
                    //   }
                    //   setState(() {});
                    // }
                    ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: _pan,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('PAN NUMBER'),
                      hintText: 'eg. ESKPL2389K',
                      // errorText:(isPan ?? true) ? null : "Pan not validator ?"
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Digit';
                      }
                      if (!RegExp(r'^[A-Z]{5}[0-9]{4}[A-Z]{1}$')
                          .hasMatch(value)) {
                        return 'Enter Valid Pan No.';
                      }

                      return null;
                    }
                    // onChanged: (value) {
                    //   if (value.isEmpty ||
                    //       RegExp(r'^[A-Z]{5}[0-9]{4}[A-Z]{1}$')
                    //           .hasMatch(value)) {
                    //     isPan = true;
                    //   } else {
                    //     isPan = false;
                    //   }
                    //   _pan.text;
                    //   setState(() {});
                    // }
                    ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _source,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Source'),
                    // hintText: 'Select an Option',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

//Adddress details
  CustomExpansionTile _addressDetails(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(
            Icons.email,
            color: Colors.white,
          ),
          SizedBox(width: 20),
          HeadlineText(
            text: "Address",
          ),
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _caddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CURRENT ADDRESS'),
                    hintText: 'Enter your current address',
                  ),
                  validator: CommonFunction.validationpage,
                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please enter some text';
                  //   }
                  //   return null;
                  // },
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _cstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _ccity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _cpincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _paddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PERMANENT ADDRESS'),
                    hintText: 'Enter your permanent address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _pstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _pcity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _ppincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

  //profesional info
  CustomExpansionTile _professionalDetails(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.cast_for_education_sharp, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(
            text: "Professional Information",
          ),
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _emptype,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'EMPLOYMENT TYPE')),
                  suggestionsCallback: (pattern) {
                    return [
                      'Salaried',
                      'Self Employed Professional',
                      'Self Employed Non-professional',
                      'Retired',
                      'Other'
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _emptype.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _employertype,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'EMPLOYER TYPE')),
                  suggestionsCallback: (pattern) {
                    return [
                      'Limited Company',
                      'Limited Liability Partnership',
                      'Partnership',
                      'Government',
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _employertype.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _companyname,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('COMPANY NAME'),
                    hintText: 'Enter company name',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _empaddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('EMPLOYER ADDRESS'),
                    hintText: 'Enter employer address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _estate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _ecity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _epincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _officialemail,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('OFFICIAL EMAIL'),
                    hintText: 'eg. vivek@easiloan.com',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _empexperience,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('OVERALL WORK EXPERIENCE(In YEARS)'),
                    hintText: 'Enter total experience in years',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _monthlyincome,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('MONTHLY INCOME'),
                    hintText: 'Enter your average net monthly income',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _existingemi,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('EXISTING EMIs'),
                    hintText: 'Enter your existing monthly EMIs',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _annualincome,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('ANNUAL INCOME'),
                    hintText: 'Enter your annual income',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _annualprofit,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('ANNUAL PROFIT'),
                    hintText: 'Enter your annual profit',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

//Loan requirements
  CustomExpansionTile _loanrequirementDetails(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.account_balance_wallet_outlined, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(
            text: "Loan Requirements",
          ),
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _product,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Product')),
                  suggestionsCallback: (pattern) {
                    return [
                      'New Home Loan',
                      'Transfer Home Loan',
                      'Loan Against Property'
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _product.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _amnt,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('AMOUNT REQUIRED'),
                    hintText: 'Enter loan amount required',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _loantenure,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('LOAN TENURE(Max:30)'),
                    hintText: 'Enter loan tenure(Max:30)',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

//property details
  CustomExpansionTile _propertyDetails(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.add_business_outlined, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(
            text: 'Property Details',
          )
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _projname,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PROJECT NAME'),
                    hintText: 'Enter project name',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _propertystatus,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'PROPERTY STATUS')),
                  suggestionsCallback: (pattern) {
                    return ['Under Construction', 'Ready to move -in'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _propertystatus.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _propertyvalue,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PROPERTY VALUE'),
                    hintText: 'Enter property value',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _pdstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _pdcity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _pdpincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

//document details
  CustomExpansionTile _docdetails(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.document_scanner_outlined, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(
            text: 'Documents',
          )
        ],
      ),
      children: [
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  style: Constants.buttonStyle,
                  // style: ElevatedButton.styleFrom(
                  //     primary: Colors.white, // background (button) color
                  //     onPrimary: const Color(0xFF05bd8e),
                  //     fixedSize: const Size(150, 40),
                  //     side: const BorderSide(color: Color(0xFF05bd8e))),
                  onPressed: () async {
                    // print('object');
                    bankfile = await pickFiles(fileType, context);
                    setState(() {});
                  },
                  child: const BoldText(
                    text: "Bank Statement",
                  ),
                ),
                ElevatedButton(
                  style: Constants.buttonStyle,
                  onPressed: () async {
                    salaryfile = await pickFiles(fileType, context);
                    setState(() {});
                  },
                  child: const BoldText(
                    text: "Salary Slip",
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      if (bankfile != null) fileDetails(bankfile!),
                      if (bankfile != null)
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: const Color(
                                0xFF152245), // background (button) color
                            onPrimary: Colors.white, // foreground (text) color
                          ),
                          onPressed: () {
                            viewFile(bankfile!);
                          },
                          child: const BoldText(
                            text: "View Selected File",
                          ),
                          // child: const Text(
                          //   'View Selected File',
                          //   style: TextStyle(
                          //     color: Colors.white,
                          //   ),
                          // ),
                        ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(children: [
                    if (salaryfile != null) fileDetails(salaryfile!),
                    if (salaryfile != null)
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: const Color(
                              0xFF152245), // background (button) color
                          onPrimary: Colors.white, // foreground (text) color
                        ),
                        onPressed: () {
                          viewFile(salaryfile!);
                        },
                        child: const BoldText(
                          text: "View Selected File",
                        ),
                      ),
                  ]),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  style: Constants.buttonStyle,
                  onPressed: () async {
                    itrfile = await pickFiles(fileType, context);
                    setState(() {});
                  },
                  child: const BoldText(
                    text: "ITR Form 16",
                  ),
                ),
                ElevatedButton(
                  style: Constants.buttonStyle,
                  onPressed: () async {
                    aadharfile = await pickFiles(fileType, context);
                    setState(() {});
                  },
                  child: const BoldText(
                    text: "Aadhar",
                  ),
                ),
              ],
            ), //view file
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      if (itrfile != null) fileDetails(itrfile!),
                      if (itrfile != null)
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: const Color(
                                0xFF152245), // background (button) color
                            onPrimary: Colors.white, // foreground (text) color
                          ),
                          onPressed: () {
                            viewFile(itrfile!);
                          },
                          child: const BoldText(
                            text: "View Selected File",
                          ),
                        ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(children: [
                    if (aadharfile != null) fileDetails(aadharfile!),
                    if (aadharfile != null)
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: const Color(
                              0xFF152245), // background (button) color
                          onPrimary: Colors.white, // foreground (text) color
                        ),
                        onPressed: () {
                          viewFile(aadharfile!);
                        },
                        child: const BoldText(
                          text: "View Selected File",
                        ),
                      ),
                  ]),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  style: Constants.buttonStyle,
                  onPressed: () async {
                    panfile = await pickFiles(fileType, context);
                    setState(() {});
                  },
                  child: const BoldText(
                    text: "Pan",
                  ),
                ),
                ElevatedButton(
                  style: Constants.buttonStyle,
                  onPressed: () async {
                    othersfile = await pickFiles(fileType, context);
                    setState(() {});
                  },
                  child: const BoldText(
                    text: "Others",
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      if (panfile != null) fileDetails(panfile!),
                      if (panfile != null)
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: const Color(
                                0xFF152245), // background (button) color
                            onPrimary: Colors.white, // foreground (text) color
                          ),
                          onPressed: () {
                            viewFile(panfile!);
                          },
                          child: const BoldText(
                            text: "View Selected File",
                          ),
                        ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: [
                      if (othersfile != null) fileDetails(othersfile!),
                      if (othersfile != null)
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: const Color(
                                0xFF152245), // background (button) color
                            onPrimary: Colors.white, // foreground (text) color
                          ),
                          onPressed: () {
                            viewFile(othersfile!);
                          },
                          child: const BoldText(
                            text: "View Selected File",
                          ),
                        ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(height: 20),
          ],
        )
      ],
    );
  }

//file size
  Widget fileDetails(PlatformFile file) {
    final kb = file.size / 1024;
    final mb = kb / 1024;
    final size = (mb >= 1)
        ? '${mb.toStringAsFixed(2)} MB'
        : '${kb.toStringAsFixed(2)} KB';
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('File Name: ${file.name}'),
          Text('File Size: $size'),
          // Text('File Extension: ${file.extension}'),
          // Text('File Path: ${file.path}'),
        ],
      ),
    );
  }

//pick file
  Future<PlatformFile?> pickFiles(
      String? filetype, BuildContext context) async {
    result = await FilePicker.platform.pickFiles();
    if (result == null) {
      return null;
    }
    return result!.files.first;
  }

  // multiple file selected
  // navigate user to 2nd screen to show selected files
  void loadSelectedFiles(List<PlatformFile> files, BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => FileList(files: files, onOpenedFile: viewFile)));
  }

  // open the picked file
  void viewFile(PlatformFile file) {
    OpenFile.open(file.path);
  }

  //calling API

  Future<void> getApiData(BuildContext context) async {
    final user = User(
      name: _name.text,
      gender: _gender.text,
      dob: _dob.text,
      marital: _marital.text,
      email: _email.text,
      mob: _mob.text,
      adhar: _adhar.text,
      pan: _pan.text,
      source: _source.text,
      caddress: _caddress.text,
      cpincode: _cpincode.text,
      cstate: _cstate.text,
      ccity: _ccity.text,
      paddress: _paddress.text,
      ppincode: _ppincode.text,
      pstate: _pstate.text,
      pcity: _pcity.text,
      emptype: _emptype.text,
      employertype: _employertype.text,
      companyname: _companyname.text,
      empaddress: _empaddress.text,
      epincode: _epincode.text,
      estate: _estate.text,
      ecity: _ecity.text,
      officialemail: _officialemail.text,
      empexperience: _empexperience.text,
      monthlyincome: _monthlyincome.text,
      existingemi: _existingemi.text,
      annualincome: _annualincome.text,
      annualprofit: _annualprofit.text,
      product: _product.text,
      amnt: _amnt.text,
      loantenure: _loantenure.text,
      projname: _projname.text,
      propertystatus: _propertystatus.text,
      propertyvalue: _propertyvalue.text,
      pdpincode: _pdpincode.text,
      pdstate: _pdstate.text,
      pdcity: _pdcity.text,
      bankdoc: _bankdoc.text,
      salaryslip: _salaryslip.text,
      itrform: _itrform.text,
      aadhardoc: _aadhardoc.text,
      pandoc: _pandoc.text,
      othersdoc: _othersdoc.text,
    );
    try {
      APIService.getApplicant(user);
      // if (respons.statusCode == 200) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => const Coapp1Details(//welcomepage
              // coapp1: coapp1,
              ),
        ),
      );
      // }
    } catch (e) {
      log("$e");
    }
  }
}
