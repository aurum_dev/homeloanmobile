import 'dart:io';
import 'package:flutter/material.dart';
import 'package:homeloan/coapp1_details.dart';
import 'package:homeloan/coapp2_details.dart';
import 'package:homeloan/dashboard_page.dart';
import 'package:homeloan/home_page.dart';
import 'package:homeloan/login_page.dart';
import 'package:homeloan/next3_page.dart';
import 'package:homeloan/register_page.dart';
import 'package:homeloan/splash_screen.dart';

void main() {
  HttpOverrides.global = MyHttpOverrides();
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: 'splashscreen',
    routes: {
      'splashscreen': (context) => const SplashScreen(),
      'register': (context) => const RegisterPage(),
      'login': (context) => const LoginPage(),
      'home': (context) => Homepage(),
      'coapp1detail': (context) => const Coapp1Details(),
      'coapp2detail': (context) => const Coapp2Details(),
      'next3page': (context) => const Next3Page(),
      'dashboard': (context) => const DashboardPage(),
      // 'welcome' :(context) => WelcomePage(),
      // 'nextpage':(context) =>  NextPage(),
      // 'next2page': (context) => const Next2Page(),
      // 'next' : (context) => NextPage(),
      // 'first' :(context) => const FirstScreen(),
    },
  ));
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
