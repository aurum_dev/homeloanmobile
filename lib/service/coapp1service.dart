import 'dart:developer';
import 'package:homeloan/models/coapp1.dart';
import 'package:http/http.dart' as http;

class APIService {
  static Future<void> getCoapplicant1(Coapp1 coapp1) async {
    log("Calling API...");
    log("Coapp1 ${coapp1.toJson()}");
    var url = Uri.parse(
      'https://vsitr_uat.recx.in/home-loan/api/homeloan',
    );
    http.Response respons = await http.post(url, body: coapp1.toJson());
    print(respons.body);

    log("API Called...");
    log(respons.body);
  }
}
