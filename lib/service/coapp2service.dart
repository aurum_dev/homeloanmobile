import 'dart:developer';
import 'package:homeloan/models/coapp2.dart';
import 'package:http/http.dart' as http;

class APIService {
  static Future<void> getCoapplicant2(Coapp2 coapp2) async {
    log("Calling API...");
    log("Coapp2 ${coapp2.toJson()}");
    var url = Uri.parse(
      'https://vsitr_uat.recx.in/home-loan/api/homeloan',
    );
    http.Response respons = await http.post(url, body: coapp2.toJson());
    print(respons.body);

    log("API Called...");
    log(respons.body);
  }
}
