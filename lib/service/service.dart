import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:homeloan/models/coapp1.dart';
import 'package:homeloan/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:path/path.dart';

class APIService {
  static Future<void> getApplicant(User user) async {
    log("Calling API...");
    log("User ${user.toJson()}");
    // log('Coapp1 ${coapp1.toJson()}'); //print
    var url = Uri.parse(
      'https://vsitr_uat.recx.in/home-loan/api/homeloan',
    );
    http.Response respons = await http.post(url, body: user.toJson());
    print(respons.body);

    log("API Called...");
    log(respons.body);
  }

  static void getCoapplicant1(Coapp1 coapp1) {}
  static Future<void> uploadDocs(
    PlatformFile? bankfile,
    PlatformFile? salaryfile,
    PlatformFile? itrfile,
    PlatformFile? aadharfile,
    PlatformFile? panfile,
    PlatformFile? othersfile,
  ) async {
    if (bankfile?.path != null ||
        salaryfile?.path != null ||
        itrfile?.path != null ||
        aadharfile?.path != null ||
        panfile?.path != null ||
        othersfile?.path != null) {
      var request = http.MultipartRequest(
        'POST',
        Uri.parse("https://vsitr_uat.recx.in/home-loan/api/homeloan"),
      );
      Map<String, String> headers = {"Content-type": "multipart/form-data"};
      request.files.add(
        http.MultipartFile(
          'image',
          File(bankfile!.path!).readAsBytes().asStream(),
          File(bankfile.path!).lengthSync(),
          filename: bankfile.path!.split('/').last,
        ),
      );

      request.files.add(
        http.MultipartFile(
          'image1',
          File(salaryfile!.path!).readAsBytes().asStream(),
          File(salaryfile.path!).lengthSync(),
          filename: salaryfile.path!.split('/').last,
        ),
      );

      request.files.add(
        http.MultipartFile(
          'image2',
          File(itrfile!.path!).readAsBytes().asStream(),
          File(itrfile.path!).lengthSync(),
          filename: itrfile.path!.split('/').last,
        ),
      );

      request.files.add(
        http.MultipartFile(
          'image3',
          File(aadharfile!.path!).readAsBytes().asStream(),
          File(aadharfile.path!).lengthSync(),
          filename: aadharfile.path!.split('/').last,
        ),
      );

      request.files.add(
        http.MultipartFile(
          'image4',
          File(panfile!.path!).readAsBytes().asStream(),
          File(panfile.path!).lengthSync(),
          filename: panfile.path!.split('/').last,
        ),
      );

      request.files.add(
        http.MultipartFile(
          'image5',
          File(othersfile!.path!).readAsBytes().asStream(),
          File(othersfile.path!).lengthSync(),
          filename: othersfile.path!.split('/').last,
        ),
      );

      request.headers.addAll(headers);
      print("request: " + request.toString());
      var res = await request.send();
      http.Response response = await http.Response.fromStream(res);
      print('response:${response.body}');
      // print("Value: ${res.body}");
      // print("Bank File: ${bankfile?.path}");
      // if (bankfile?.path != null ||
      //     salaryfile?.path != null ||
      //     itrfile?.path != null ||
      //     aadharfile?.path != null ||
      //     panfile?.path != null ||
      //     othersfile?.path != null) {
      //   final selectedBankImage = File(bankfile!.path!); //bankfile
      //   final selectedSalaryImage = File(salaryfile!.path!); //salary file
      //   final selectedItrformImage = File(itrfile!.path!); //ITR Form file
      //   final selectedAadharImage = File(aadharfile!.path!); //aadhar file
      //   final selectedPanImage = File(panfile!.path!); //pan file
      //   final selectedOthersImage = File(othersfile!.path!); //others file

      //   var bankstream =
      //       http.ByteStream(DelegatingStream.typed(selectedBankImage.openRead()));
      //   var salarystream = http.ByteStream(
      //       DelegatingStream.typed(selectedSalaryImage.openRead()));
      //   var itrformstream = http.ByteStream(
      //       DelegatingStream.typed(selectedItrformImage.openRead()));
      //   var aadharstream = http.ByteStream(
      //       DelegatingStream.typed(selectedSalaryImage.openRead()));
      //   var panstream = http.ByteStream(
      //       DelegatingStream.typed(selectedAadharImage.openRead()));
      //   var othersstream = http.ByteStream(
      //       DelegatingStream.typed(selectedOthersImage.openRead()));
      //   //add

      //   // get file length
      //   var length =
      //       await selectedBankImage.length(); //imageFile is your image file//add
      //   Map<String, String> headers = {
      //     "Accept": "multipart/form-data",
      //   }; // ignore this headers if there is no authentication

      //   // string to uri
      //   var uri = Uri.parse("https://vsitr_uat.recx.in/home-loan/api/homeloan");

      //   // create multipart request
      //   var request = http.MultipartRequest("POST", uri);
      //   print("Headers: ${headers}, URI:${request.url}");
      //   // multipart that takes file
      //   var multipartFileSign = http.MultipartFile('bank_pic', bankstream, length,
      //       filename: basename(selectedBankImage.path));
      //   var multipartSalaryFileSign = http.MultipartFile(
      //       'salary_pic', salarystream, length,
      //       filename: basename(selectedSalaryImage.path));
      //   var multipartItrformFileSign = http.MultipartFile(
      //       'itrform_pic', itrformstream, length,
      //       filename: basename(selectedItrformImage.path));
      //   var multipartAadharFileSign = http.MultipartFile(
      //       'aadhar_pic', aadharstream, length,
      //       filename: basename(selectedAadharImage.path));
      //   var multipartPanFileSign = http.MultipartFile(
      //       'pan_pic', panstream, length,
      //       filename: basename(selectedPanImage.path));
      //   var multipartOthersFileSign = http.MultipartFile(
      //       'Others_pic', othersstream, length,
      //       filename: basename(selectedOthersImage.path));

      //   // add file to multipart
      //   request.files.add(multipartFileSign);
      //   request.files.add(multipartSalaryFileSign);
      //   request.files.add(multipartItrformFileSign);
      //   request.files.add(multipartAadharFileSign);
      //   request.files.add(multipartPanFileSign);
      //   request.files.add(multipartOthersFileSign);

      //   //add headers
      //   request.headers.addAll(headers);
      //   print("Files Length ${request.files.length}");

      //   print("request: " + request.toString());
      //   var res = await request.send();
      //   http.Response response = await http.Response.fromStream(res);
      //   print("Response: ${response.body}");
      //   // // send
      //   // var response = await request.send();

      //   // print("Status Code ${response.statusCode}");

      //   // // listen for response
      //   // response.stream.transform(utf8.decoder).listen((value) {
      //   //   print("Response Stream${value}");
      //   // });
      //   // request.send().then((response) {
      //   //   print("Response ${response.statusCode}");
      //   //   if (response.statusCode == 200) print("Uploaded!");
      //   // });
      //   // http.Response res = await http.Response.fromStream(response);
      //   // print("Value: ${res.body}");
      // }
    }
  }
}
