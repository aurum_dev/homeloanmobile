import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:homeloan/coapp2_details.dart';
import 'package:homeloan/login_page.dart';
import 'package:homeloan/models/coapp1.dart';
import 'package:homeloan/service/coapp1service.dart';
import 'package:homeloan/widgets/Custom_Expansion_Tile.dart';
import 'package:homeloan/widgets/Validation_Page.dart';
import 'package:homeloan/widgets/drawer.dart';
import 'package:homeloan/widgets/headline_text.dart';

class Coapp1Details extends StatefulWidget {
  const Coapp1Details({Key? key}) : super(key: key);

  @override
  State<Coapp1Details> createState() => _Coapp1DetailsState();
}

class _Coapp1DetailsState extends State<Coapp1Details> {
  TextEditingController _coapp1name = TextEditingController();
  TextEditingController _coapp1gender = TextEditingController();
  TextEditingController _coapp1dob = TextEditingController();
  TextEditingController _coapp1marital = TextEditingController();
  TextEditingController _coapp1email = TextEditingController();
  TextEditingController _coapp1mob = TextEditingController();
  TextEditingController _coapp1adhar = TextEditingController();
  TextEditingController _coapp1pan = TextEditingController();
  TextEditingController _coapp1relation =
      TextEditingController(); //personal info

  TextEditingController _coapp1caddress = TextEditingController();
  TextEditingController _coapp1cpincode = TextEditingController();
  TextEditingController _coapp1cstate = TextEditingController();
  TextEditingController _coapp1ccity =
      TextEditingController(); // current address

  TextEditingController _coapp1paddress = TextEditingController();
  TextEditingController _coapp1ppincode = TextEditingController();
  TextEditingController _coapp1pstate = TextEditingController();
  TextEditingController _coapp1pcity =
      TextEditingController(); //permanent address

  TextEditingController _coapp1emptype = TextEditingController();
  TextEditingController _coapp1employertype = TextEditingController();
  TextEditingController _coapp1companyname = TextEditingController();
  TextEditingController _coapp1empaddress = TextEditingController();
  TextEditingController _coapp1epincode = TextEditingController();
  TextEditingController _coapp1estate = TextEditingController();
  TextEditingController _coapp1ecity = TextEditingController();
  TextEditingController _coapp1officialemail = TextEditingController();
  TextEditingController _coapp1empexperience = TextEditingController();
  TextEditingController _coapp1monthlyincome = TextEditingController();
  TextEditingController _coapp1existingemi = TextEditingController();
  TextEditingController _coapp1annualincome = TextEditingController();
  TextEditingController _coapp1annualprofit = TextEditingController(); //Prfessi

  // TextEditingController _typeAheadController = TextEditingController();
  // TextEditingController _typeMaritalAheadController = TextEditingController();

  bool? isAdhar;
  bool? isPan;
  bool? isMbl;

  var adharValue;

  final formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf3fbfe),
      key: _key,
      endDrawer: const MyDrawer(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Container(
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: const Color(0xFFf3fbfe),
                // borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: const Offset(1, 1)),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'assets/images/KuberX.png',
                    width: 80,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: InkWell(
                          onTap: () {
                            _key.currentState!.openEndDrawer();
                          },
                          child: const Icon(Icons.menu))),
                ],
              ),
            ),
            Form(key: formKey, child: _buildBody(context)),
          ]),
        ),
      ),
    );
  }

  SingleChildScrollView _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 20),
          const Text(
            'Co-Applicant1',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          _generalCoApp1Details(context),
          const SizedBox(height: 10),
          _addressCoApp1Details(context),
          const SizedBox(height: 10),
          _professionalCoApp1Details(context),
          const SizedBox(height: 10),

/*=============================================two button=============================================*/
          const SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary:
                        const Color(0xFF152245), // background (button) color
                    onPrimary: Colors.white, // foreground (text) color
                    fixedSize: const Size(150, 40)),
                // autofocus:false,
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const LoginPage()));
                },
                child: const Text(
                  'CANCEL',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(width: 40),
              ElevatedButton(
                // autofocus: false,
                style: ElevatedButton.styleFrom(
                    primary:
                        const Color(0xFF152245), // background (button) color
                    onPrimary: Colors.white, // foreground (text) color
                    fixedSize: const Size(150, 40)),
                onPressed: () {
                  isPan ??= false;
                  log("PAN: $isPan"); //pan

                  isAdhar ??= false;
                  log("Aadhar: $isAdhar"); // aadhar

                  isMbl ??= false;
                  log("Mobile: $isMbl"); //mobile

                  setState(() {});
                  log("${formKey.currentState!.validate()}");

                  if (formKey.currentState!.validate()) {
                    getApicoapp1(); //called api

                  } else {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                          title: const Text('AlertDialog Title'),
                          content: const Text('All Fields Required'),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.pop(context, 'OK');
                                },
                                child: const Text('OK',
                                    style: TextStyle(
                                      color: Color(0xFF152245),
                                    ))),
                          ]),
                    );
                  }
                },
                child: const Text(
                  'SUBMIT',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  //General Details
  CustomExpansionTile _generalCoApp1Details(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.people, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(text: 'General Details')
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1name,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Full Name'),
                    hintText: 'Enter Full Name',
                  ),
                  validator: CommonFunction.validationpage,
                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please enter some text';
                  //   }
                  //   return null;
                  // },
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp1gender,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Gender')),
                  suggestionsCallback: (pattern) {
                    return ['Female', 'Male', 'Others'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp1gender.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              // ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  onTap: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1982),
                      lastDate: DateTime(2023),
                    );
                    // intl
                    _coapp1dob.text = "$date";
                  },
                  controller: _coapp1dob,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('D.O.B'),
                    hintText: 'Enter Date of Birth',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp1marital,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Marital Status')),
                  suggestionsCallback: (pattern) {
                    return ['Married', 'Single', 'Divorced'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp1marital.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  // keyboardType: TextInputType.emailAddress,
                  controller: _coapp1email,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Email'),
                    hintText: 'Report will be sent here',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    // keyboardType: TextInputType.number,
                    controller: _coapp1mob,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        label: const Text('Mobile No.'),
                        hintText: 'Enter Mobile No.',
                        errorText:
                            (isMbl ?? true) ? null : "Invalid Mobile No.?"),
                    onChanged: (value) {
                      if (value.isEmpty) {
                        isMbl = false;
                      }
                      if (RegExp(r'^[0-9]{10}$').hasMatch(value)) {
                        isMbl = true;
                      } else {
                        isMbl = false;
                      }

                      // _coapp1mob.text;
                      // setState(() {});
                    }),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    // keyboardType: TextInputType.number,
                    controller: _coapp1adhar,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        label: const Text('Aadhar No.'),
                        hintText: 'Enter 12 digit aadhar number',
                        errorText: (isAdhar ?? true)
                            ? null
                            : "Aadhar not validator ?"),
                    onChanged: (value) {
                      // String patttern = r'/^[2-9]{1}[0-9]{3}\s{1}[0-9]{4}\s{1}[0-9]{4}$/';
                      if (value.isEmpty ||
                          RegExp(r'^[2-9]{1}[0-9]{11}$').hasMatch(value)) {
                        isAdhar = true;
                      } else {
                        isAdhar = false;
                      }
                      // _coapp1adhar.text;
                      // setState(() {});
                    }),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: _coapp1pan,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        label: const Text('PAN NUMBER'),
                        hintText: 'eg. ESKPL2389K',
                        errorText:
                            (isPan ?? true) ? null : "Pan not validator ?"),
                    onChanged: (value) {
                      if (value.isEmpty ||
                          RegExp(r'^[A-Z]{5}[0-9]{4}[A-Z]{1}$')
                              .hasMatch(value)) {
                        isPan = true;
                      } else {
                        isPan = false;
                      }
                      // _coapp1pan.text;
                      // setState(() {});
                    }),
              ),
              const SizedBox(height: 20), //relationship

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp1relation,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'RELATIONSHIP')),
                  suggestionsCallback: (pattern) {
                    return [
                      'Mother',
                      'Father',
                      'Spouse',
                      'Brother',
                      'Sister',
                      'Other'
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp1relation.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),

              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

//Adddress details

  CustomExpansionTile _addressCoApp1Details(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.email, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(text: 'Address')
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1caddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CURRENT ADDRESS'),
                    hintText: 'Enter your current address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1cstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1ccity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1cpincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1paddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PERMANENT ADDRESS'),
                    hintText: 'Enter your permanent address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1pstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1pcity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1ppincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

  //profesional info
  CustomExpansionTile _professionalCoApp1Details(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.cast_for_education_sharp, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(text: 'Professional Information')
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp1emptype,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'EMPLOYMENT TYPE')),
                  suggestionsCallback: (pattern) {
                    return [
                      'Salaried',
                      'Self Employed Professional',
                      'Self Employed Non-professional',
                      'Retired',
                      'Other'
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp1emptype.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp1employertype,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'EMPLOYER TYPE')),
                  suggestionsCallback: (pattern) {
                    return [
                      'Limited Company',
                      'Limited Liability Partnership',
                      'Partnership',
                      'Government',
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp1employertype.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1companyname,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('COMPANY NAME'),
                    hintText: 'Enter company name',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1empaddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('EMPLOYER ADDRESS'),
                    hintText: 'Enter employer address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1estate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1ecity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1epincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1officialemail,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('OFFICIAL EMAIL'),
                    hintText: 'eg. vivek@easiloan.com',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1empexperience,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('OVERALL WORK EXPERIENCE(In YEARS)'),
                    hintText: 'Enter total experience in years',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1monthlyincome,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('MONTHLY INCOME'),
                    hintText: 'Enter your average net monthly income',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1existingemi,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('EXISTING EMIs'),
                    hintText: 'Enter your existing monthly EMIs',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1annualincome,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('ANNUAL INCOME'),
                    hintText: 'Enter your annual income',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp1annualprofit,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('ANNUAL PROFIT'),
                    hintText: 'Enter your annual profit',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

  Future<void> getApicoapp1() async {
    final coapp1 = Coapp1(
        coapp1name: _coapp1name.text,
        coapp1gender: _coapp1gender.text,
        coapp1dob: _coapp1dob.text,
        coapp1marital: _coapp1marital.text,
        coapp1email: _coapp1email.text,
        coapp1mob: _coapp1mob.text,
        coapp1adhar: _coapp1adhar.text,
        coapp1pan: _coapp1pan.text,
        coapp1relation: _coapp1relation.text,
        coapp1caddress: _coapp1caddress.text,
        coapp1cpincode: _coapp1cpincode.text,
        coapp1cstate: _coapp1cstate.text,
        coapp1ccity: _coapp1ccity.text,
        coapp1paddress: _coapp1paddress.text,
        coapp1ppincode: _coapp1ppincode.text,
        coapp1pstate: _coapp1pstate.text,
        coapp1pcity: _coapp1pcity.text,
        coapp1emptype: _coapp1emptype.text,
        coapp1employertype: _coapp1employertype.text,
        coapp1companyname: _coapp1companyname.text,
        coapp1empaddress: _coapp1empaddress.text,
        coapp1epincode: _coapp1epincode.text,
        coapp1estate: _coapp1estate.text,
        coapp1ecity: _coapp1ecity.text,
        coapp1officialemail: _coapp1officialemail.text,
        coapp1empexperience: _coapp1empexperience.text,
        coapp1monthlyincome: _coapp1monthlyincome.text,
        coapp1existingemi: _coapp1existingemi.text,
        coapp1annualincome: _coapp1annualincome.text,
        coapp1annualprofit: _coapp1annualprofit.text);
    try {
      APIService.getCoapplicant1(coapp1);
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => const Coapp2Details(),
        ),
      );
    } catch (e) {
      log("$e");
    }
  }
}



//   @override
//   Widget build(BuildContext context) {
//     return Container(
      
//     );
//   }
// }