import 'dart:async';
import 'package:flutter/material.dart';
import 'package:homeloan/splash_screen.dart';

class Next3Page extends StatefulWidget {
  const Next3Page({Key? key}) : super(key: key);

  @override
  State<Next3Page> createState() => _Next3PageState();
}

class _Next3PageState extends State<Next3Page> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(seconds: 5),
        () => Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const SplashScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf3fbfe),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
            alignment: Alignment.center,
            child: const Text(
              'Thank You',
              style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
            )),
      ),
    );
  }
}
