import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:homeloan/already_account.dart';
import 'package:homeloan/login_page.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  var confirmPass;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf3fbfe),
      body: ListView(
        children: [
          Container(
            width: 150,
            height: 150,
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 40),
            child: Image.asset(
              'assets/images/KuberX.png',
              width: 150,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 35),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Text(
                  'REGISTER',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          SizedBox(height: 30),
          Form(
            key: formkey,
            child: Container(
              padding: EdgeInsets.only(
                  // top: MediaQuery.of(context).size.height * 0.1,
                  right: 35,
                  left: 35),
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Name',
                        hintText: 'Please Enter Your Name'),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Your Name';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email',
                          hintText: 'Enter valid email id as abc@gmail.com'),
                      validator: MultiValidator([
                        RequiredValidator(errorText: "Please Enter Your Email"),
                        EmailValidator(errorText: "Enter valid email id"),
                      ])),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                        hintText: 'Enter secure password'),
                    validator: (value) {
                      confirmPass = value;
                      if (value == null || value.isEmpty) {
                        return 'Please Enter New Password';
                      } else if (value.length < 6) {
                        return "Password must be atleast 6 characters long";
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Confirm Password',
                        hintText: 'Enter secure password'),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Re-Enter New Password';
                      } else if (value.length < 6) {
                        return "Password must be atleast 6 characters long";
                      } else if (value != confirmPass) {
                        return "Password must be same as above";
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(height: 15),
                  Container(
                    // width: 300,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary:
                              Color(0xFF152245), // background (button) color
                          onPrimary: Colors.white, // foreground (text) color
                          fixedSize: Size(400, 50)),
                      onPressed: (() {
                        if (formkey.currentState!.validate()) {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) => LoginPage()));
                          print("Validated");
                        } else {
                          print("Not Validated");
                        }
                      }),
                      child: Text(
                        'SIGN UP',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                  AlreadyAccount(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
