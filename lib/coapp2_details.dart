import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:homeloan/login_page.dart';
import 'package:homeloan/models/coapp2.dart';
import 'package:homeloan/next3_page.dart';
import 'package:homeloan/service/coapp2service.dart';
import 'package:homeloan/widgets/Custom_Expansion_Tile.dart';
import 'package:homeloan/widgets/Validation_Page.dart';
import 'package:homeloan/widgets/drawer.dart';
import 'package:homeloan/widgets/headline_text.dart';

class Coapp2Details extends StatefulWidget {
  const Coapp2Details({Key? key}) : super(key: key);

  @override
  State<Coapp2Details> createState() => _Coapp2DetailsState();
}

class _Coapp2DetailsState extends State<Coapp2Details> {
  TextEditingController _coapp2name = TextEditingController();
  TextEditingController _coapp2gender = TextEditingController();
  TextEditingController _coapp2dob = TextEditingController();
  TextEditingController _coapp2marital = TextEditingController();
  TextEditingController _coapp2email = TextEditingController();
  TextEditingController _coapp2mob = TextEditingController();
  TextEditingController _coapp2adhar = TextEditingController();
  TextEditingController _coapp2pan = TextEditingController();
  TextEditingController _coapp2relation =
      TextEditingController(); //personal info

  TextEditingController _coapp2caddress = TextEditingController();
  TextEditingController _coapp2cpincode = TextEditingController();
  TextEditingController _coapp2cstate = TextEditingController();
  TextEditingController _coapp2ccity =
      TextEditingController(); // current address

  TextEditingController _coapp2paddress = TextEditingController();
  TextEditingController _coapp2ppincode = TextEditingController();
  TextEditingController _coapp2pstate = TextEditingController();
  TextEditingController _coapp2pcity =
      TextEditingController(); //permanent address

  TextEditingController _coapp2emptype = TextEditingController();
  TextEditingController _coapp2employertype = TextEditingController();
  TextEditingController _coapp2companyname = TextEditingController();
  TextEditingController _coapp2empaddress = TextEditingController();
  TextEditingController _coapp2epincode = TextEditingController();
  TextEditingController _coapp2estate = TextEditingController();
  TextEditingController _coapp2ecity = TextEditingController();
  TextEditingController _coapp2officialemail = TextEditingController();
  TextEditingController _coapp2empexperience = TextEditingController();
  TextEditingController _coapp2monthlyincome = TextEditingController();
  TextEditingController _coapp2existingemi = TextEditingController();
  TextEditingController _coapp2annualincome = TextEditingController();
  TextEditingController _coapp2annualprofit =
      TextEditingController(); //Prfessional Info

  // TextEditingController _typeAheadController = TextEditingController();
  // TextEditingController _typeMaritalAheadController = TextEditingController();

  bool? isAdhar;
  bool? isPan;
  bool isName = true;
  bool? isMbl;

  final formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf3fbfe),
      key: _key,
      endDrawer: const MyDrawer(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Container(
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: const Color(0xFFf3fbfe),
                // borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: const Offset(1, 1)),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'assets/images/KuberX.png',
                    width: 80,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: InkWell(
                          onTap: () {
                            _key.currentState!.openEndDrawer();
                          },
                          child: const Icon(Icons.menu))),
                ],
              ),
            ),
            Form(key: formKey, child: _buildBody(context)),
          ]),
        ),
      ),
    );
  }

  SingleChildScrollView _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 20),
          const Text(
            'Co-Applicant2',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          _generalCoApp2Details(context),
          const SizedBox(height: 10),
          _addressCoApp2Details(context),
          const SizedBox(height: 10),
          _professionalCoApp2Details(context),
          const SizedBox(height: 10),

/*=============================================two button=============================================*/
          const SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary:
                        const Color(0xFF152245), // background (button) color
                    onPrimary: Colors.white, // foreground (text) color
                    fixedSize: const Size(150, 40)),
                // autofocus:false,
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const LoginPage()));
                },
                child: const Text(
                  'CANCEL',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(width: 40),
              ElevatedButton(
                // autofocus: false,
                style: ElevatedButton.styleFrom(
                    primary:
                        const Color(0xFF152245), // background (button) color
                    onPrimary: Colors.white, // foreground (text) color
                    fixedSize: const Size(150, 40)),
                onPressed: () {
                  isPan ??= false;
                  log("PAN: $isPan"); //pan

                  isAdhar ??= false;
                  log("Aadhar: $isAdhar"); // aadhar

                  isMbl ??= false;
                  log("Mobile: $isMbl"); //mobile

                  setState(() {});
                  log("${formKey.currentState!.validate()}");

                  if (formKey.currentState!.validate()) {
                    getApicoapp2(); //called api

                  } else {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                          title: const Text('AlertDialog Title'),
                          content: const Text('All Fields Required'),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.pop(context, 'OK');
                                },
                                child: const Text('OK',
                                    style: TextStyle(
                                      color: Color(0xFF152245),
                                    ))),
                          ]),
                    );
                  }
                },
                child: const Text(
                  'SUBMIT',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  //General Details

  CustomExpansionTile _generalCoApp2Details(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.people, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(text: 'General Details')
          // Icon(Icons.people)
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2name,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Full Name'),
                    hintText: 'Enter Full Name',
                  ),
                  validator: CommonFunction.validationpage,
                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please enter some text';
                  //   }
                  //   return null;
                  // },
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp2gender,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Gender')),
                  suggestionsCallback: (pattern) {
                    return ['Female', 'Male', 'Others'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp2gender.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              // ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  onTap: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1982),
                      lastDate: DateTime(2023),
                    );
                    // intl
                    _coapp2dob.text = "$date";
                  },
                  controller: _coapp2dob,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('D.O.B'),
                    hintText: 'Enter Date of Birth',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp2marital,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Marital Status')),
                  suggestionsCallback: (pattern) {
                    return ['Married', 'Single', 'Divorced'];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp2marital.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2email,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('Email'),
                    hintText: 'Report will be sent here',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: _coapp2mob,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        label: const Text('Mobile No.'),
                        hintText: 'Enter Mobile No.',
                        errorText:
                            (isMbl ?? true) ? null : "Invalid Mobile No.?"),
                    onChanged: (value) {
                      if (value.isEmpty) {
                        isMbl = false;
                      }
                      if (RegExp(r'^[0-9]{10}$').hasMatch(value)) {
                        isMbl = true;
                      } else {
                        isMbl = false;
                      }
                      // _coapp2mob.text;
                      setState(() {});
                    }),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: _coapp2adhar,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        label: const Text('Aadhar No.'),
                        hintText: 'Enter 12 digit aadhar number',
                        errorText: (isAdhar ?? true)
                            ? null
                            : "Aadhar not validator ?"),
                    onChanged: (value) {
                      // String patttern = r'/^[2-9]{1}[0-9]{3}\s{1}[0-9]{4}\s{1}[0-9]{4}$/';
                      if (value.isEmpty ||
                          RegExp(r'^[2-9]{1}[0-9]{11}$').hasMatch(value)) {
                        isAdhar = true;
                      } else {
                        isAdhar = false;
                      }
                      // _coapp2adhar.text;
                      setState(() {});
                    }),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: _coapp2pan,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        label: const Text('PAN NUMBER'),
                        hintText: 'eg. ESKPL2389K',
                        errorText:
                            (isPan ?? true) ? null : "Pan not validator ?"),
                    onChanged: (value) {
                      if (value.isEmpty ||
                          RegExp(r'^[A-Z]{5}[0-9]{4}[A-Z]{1}$')
                              .hasMatch(value)) {
                        isPan = true;
                      } else {
                        isPan = false;
                      }
                      // _coapp2pan.text;
                      setState(() {});
                    }),
              ),
              const SizedBox(height: 20),
              //relationship
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _coapp2relation,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'RELATIONSHIP')),
                  suggestionsCallback: (pattern) {
                    return [
                      'Mother',
                      'Father',
                      'Spouse',
                      'Brother',
                      'Sister',
                      'Other'
                    ];
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text('$suggestion'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _coapp2relation.text = "$suggestion";
                  },
                  validator: DropFunction.dropvalidation,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }
  
//Adddress details
  CustomExpansionTile _addressCoApp2Details(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: const [
          Icon(Icons.email, color: Colors.white),
          SizedBox(width: 20),
          HeadlineText(text: 'Address')
        ],
      ),
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2caddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CURRENT ADDRESS'),
                    hintText: 'Enter your current address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2cstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2ccity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2cpincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2paddress,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PERMANENT ADDRESS'),
                    hintText: 'Enter your permanent address',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2pstate,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('STATE'),
                    hintText: 'State will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2pcity,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('CITY'),
                    hintText: 'City will be based on your pincode',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _coapp2ppincode,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text('PINCODE'),
                    hintText: 'Enter locality',
                  ),
                  validator: CommonFunction.validationpage,
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
      ],
    );
  }

  //profesional info
  CustomExpansionTile _professionalCoApp2Details(BuildContext context) {
    return CustomExpansionTile(
        title: Row(
          children: const [
            Icon(Icons.cast_for_education_sharp, color: Colors.white),
            SizedBox(
              width: 20,
            ),
            HeadlineText(text: 'Professional Information')
          ],
        ),
        children: [
          Container(
            color: Colors.white,
            child: Column(
              children: [
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TypeAheadFormField(
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _coapp2emptype,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'EMPLOYMENT TYPE')),
                    suggestionsCallback: (pattern) {
                      return [
                        'Salaried',
                        'Self Employed Professional',
                        'Self Employed Non-professional',
                        'Retired',
                        'Other'
                      ];
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        title: Text('$suggestion'),
                      );
                    },
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      _coapp2emptype.text = "$suggestion";
                    },
                    validator: DropFunction.dropvalidation,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TypeAheadFormField(
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _coapp2employertype,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'EMPLOYER TYPE')),
                    suggestionsCallback: (pattern) {
                      return [
                        'Limited Company',
                        'Limited Liability Partnership',
                        'Partnership',
                        'Government',
                      ];
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        title: Text('$suggestion'),
                      );
                    },
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      _coapp2employertype.text = "$suggestion";
                    },
                    validator: DropFunction.dropvalidation,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2companyname,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('COMPANY NAME'),
                      hintText: 'Enter company name',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2empaddress,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('EMPLOYER ADDRESS'),
                      hintText: 'Enter employer address',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2estate,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('STATE'),
                      hintText: 'State will be based on your pincode',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2ecity,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('CITY'),
                      hintText: 'City will be based on your pincode',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2epincode,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('PINCODE'),
                      hintText: 'Enter locality',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2officialemail,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('OFFICIAL EMAIL'),
                      hintText: 'eg. vivek@easiloan.com',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2empexperience,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('OVERALL WORK EXPERIENCE(In YEARS)'),
                      hintText: 'Enter total experience in years',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2monthlyincome,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('MONTHLY INCOME'),
                      hintText: 'Enter your average net monthly income',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2existingemi,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('EXISTING EMIs'),
                      hintText: 'Enter your existing monthly EMIs',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2annualincome,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('ANNUAL INCOME'),
                      hintText: 'Enter your annual income',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _coapp2annualprofit,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      label: Text('ANNUAL PROFIT'),
                      hintText: 'Enter your annual profit',
                    ),
                    validator: CommonFunction.validationpage,
                  ),
                ),
                const SizedBox(height: 20),
              ],
            ),
          )
        ]);
  }

  Future<void> getApicoapp2() async {
    final coapp2 = Coapp2(
        coapp2name: _coapp2name.text,
        coapp2gender: _coapp2gender.text,
        coapp2dob: _coapp2dob.text,
        coapp2marital: _coapp2marital.text,
        coapp2email: _coapp2email.text,
        coapp2mob: _coapp2mob.text,
        coapp2adhar: _coapp2adhar.text,
        coapp2pan: _coapp2pan.text,
        coapp2relation: _coapp2relation.text,
        coapp2caddress: _coapp2caddress.text,
        coapp2cpincode: _coapp2cpincode.text,
        coapp2cstate: _coapp2cstate.text,
        coapp2ccity: _coapp2ccity.text,
        coapp2paddress: _coapp2paddress.text,
        coapp2ppincode: _coapp2ppincode.text,
        coapp2pstate: _coapp2pstate.text,
        coapp2pcity: _coapp2pcity.text,
        coapp2emptype: _coapp2emptype.text,
        coapp2employertype: _coapp2employertype.text,
        coapp2companyname: _coapp2companyname.text,
        coapp2empaddress: _coapp2empaddress.text,
        coapp2epincode: _coapp2epincode.text,
        coapp2estate: _coapp2estate.text,
        coapp2ecity: _coapp2ecity.text,
        coapp2officialemail: _coapp2officialemail.text,
        coapp2empexperience: _coapp2empexperience.text,
        coapp2monthlyincome: _coapp2monthlyincome.text,
        coapp2existingemi: _coapp2existingemi.text,
        coapp2annualincome: _coapp2annualincome.text,
        coapp2annualprofit: _coapp2annualprofit.text);

    try {
      APIService.getCoapplicant2(coapp2);
      // if (respons.statusCode == 200) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => const Next3Page(),
        ),
      );
      // }
    } catch (e) {
      log("$e");
    }
  }
}
