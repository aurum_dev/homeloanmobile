class Coapp2 {
  final String coapp2name, coapp2gender, coapp2dob, coapp2marital, coapp2email, coapp2mob, coapp2adhar, coapp2pan, coapp2relation;
  final String coapp2caddress, coapp2cpincode, coapp2cstate, coapp2ccity, coapp2paddress, coapp2ppincode, coapp2pstate, coapp2pcity;
  final String coapp2emptype,coapp2employertype, coapp2companyname, coapp2empaddress, coapp2epincode, coapp2estate, coapp2ecity, coapp2officialemail, coapp2empexperience,     coapp2monthlyincome, coapp2existingemi, coapp2annualincome, coapp2annualprofit;
  
 

  Coapp2(
      {required this.coapp2name,
      required this.coapp2gender,
      required this.coapp2dob,
      required this.coapp2marital,
      required this.coapp2email,
      required this.coapp2mob,
      required this.coapp2adhar,
      required this.coapp2pan,
      required this.coapp2relation,
      required this.coapp2caddress,
      required this.coapp2cpincode,
      required this.coapp2cstate,
      required this.coapp2ccity,
      required this.coapp2paddress,
      required this.coapp2ppincode,
      required this.coapp2pstate,
      required this.coapp2pcity,
      required this.coapp2emptype,
      required this.coapp2employertype,
      required this.coapp2companyname,
      required this.coapp2empaddress,
      required this.coapp2epincode,
      required this.coapp2estate,
      required this.coapp2ecity,
      required this.coapp2officialemail,
      required this.coapp2empexperience,
      required this.coapp2monthlyincome,
      required this.coapp2existingemi,
      required this.coapp2annualincome,
      required this.coapp2annualprofit,      
      });

  Map<String, dynamic> toJson() {
    return {
      "name":coapp2name,
      "gender":coapp2gender,
      "dob":coapp2dob,
      "marital":coapp2marital,
      "email":coapp2email,
      "mob":coapp2mob,
      "adhar":coapp2adhar,
      "pan":coapp2pan,
      "relationship":coapp2relation,
      "caddress":coapp2caddress,
      "cpincode":coapp2cpincode,
      "cstate":coapp2cstate,
      "ccity":coapp2ccity,
      "paddress":coapp2paddress,
      "ppincode":coapp2ppincode,
      "pstate":coapp2pstate,
      "pcity":coapp2pcity,
      "emptype":coapp2emptype,
      "empployertype":coapp2employertype,
      "companyname":coapp2companyname,
      "empaddress":coapp2empaddress,      
      "epincode":coapp2epincode,
      "estate":coapp2estate,
      "ecity":coapp2ecity,
      "officialemail":coapp2officialemail,
      "empexperience":coapp2empexperience,
      "monthlyincome":coapp2monthlyincome,
      "existingemi":coapp2existingemi,
      "annualincome":coapp2annualincome,
      "annualprofit":coapp2annualprofit,      
    };
  }
}
