class Coapp1 {
  final String coapp1name, coapp1gender, coapp1dob, coapp1marital, coapp1email, coapp1mob, coapp1adhar, coapp1pan, coapp1relation;
  final String coapp1caddress, coapp1cpincode, coapp1cstate, coapp1ccity, coapp1paddress, coapp1ppincode, coapp1pstate, coapp1pcity;
  final String coapp1emptype,coapp1employertype, coapp1companyname, coapp1empaddress, coapp1epincode, coapp1estate, coapp1ecity, coapp1officialemail, coapp1empexperience,     coapp1monthlyincome, coapp1existingemi, coapp1annualincome, coapp1annualprofit;
  
 

  Coapp1(
      {required this.coapp1name,
      required this.coapp1gender,
      required this.coapp1dob,
      required this.coapp1marital,
      required this.coapp1email,
      required this.coapp1mob,
      required this.coapp1adhar,
      required this.coapp1pan,
      required this.coapp1relation,
      required this.coapp1caddress,
      required this.coapp1cpincode,
      required this.coapp1cstate,
      required this.coapp1ccity,
      required this.coapp1paddress,
      required this.coapp1ppincode,
      required this.coapp1pstate,
      required this.coapp1pcity,
      required this.coapp1emptype,
      required this.coapp1employertype,
      required this.coapp1companyname,
      required this.coapp1empaddress,
      required this.coapp1epincode,
      required this.coapp1estate,
      required this.coapp1ecity,
      required this.coapp1officialemail,
      required this.coapp1empexperience,
      required this.coapp1monthlyincome,
      required this.coapp1existingemi,
      required this.coapp1annualincome,
      required this.coapp1annualprofit,      
      });

  Map<String, dynamic> toJson() {
    return {
      "name":coapp1name,
      "gender":coapp1gender,
      "dob":coapp1dob,
      "marital":coapp1marital,
      "email":coapp1email,
      "mob":coapp1mob,
      "adhar":coapp1adhar,
      "pan":coapp1pan,
      "relationship":coapp1relation,
      "caddress":coapp1caddress,
      "cpincode":coapp1cpincode,
      "cstate":coapp1cstate,
      "ccity":coapp1ccity,
      "paddress":coapp1paddress,
      "ppincode":coapp1ppincode,
      "pstate":coapp1pstate,
      "pcity":coapp1pcity,
      "emptype":coapp1emptype,
      'employertype':coapp1employertype,
      "companyname":coapp1companyname,
      "empaddress":coapp1empaddress,      
      "epincode":coapp1epincode,
      "estate":coapp1estate,
      "ecity":coapp1ecity,
      "officialemail":coapp1officialemail,
      "empexperience":coapp1empexperience,
      "monthlyincome":coapp1monthlyincome,
      "existingemi":coapp1existingemi,
      "annualincome":coapp1annualincome,
      "annualprofit":coapp1annualprofit,      
    };
  }
}
