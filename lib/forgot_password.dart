import 'package:flutter/material.dart';

class ForgotPassword extends StatelessWidget {
  const ForgotPassword({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children:const [
        Text(
          'FORGOT PASSWORD?',
          style: TextStyle(
            color: Color(0xFF05bd8e),
            decoration: TextDecoration.underline,
            fontWeight: FontWeight.bold
          ),
        )
      ],
    );
  }
}
